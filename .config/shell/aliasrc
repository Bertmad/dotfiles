# Use neovim for vim if present.
[ -x "$(command -v neovide)" ] && alias vim="neovide" vimdiff="nvim -d"

# Use $XINITRC variable if file exists.
[ -f "$XINITRC" ] && alias startx="startx $XINITRC"

# Use alias for doas if present, otherwise use it for sudo
[ -x "$(command -v doas)" ] && alias please="doas --" || alias please="sudo"

# For fixing missing XDG standards
alias \
	gdb="gdb -nh -x "${XDG_CONFIG_HOME:-$HOME/.config}"/gdb/init"

# Verbosity and settings that you pretty much just always are going to want.
alias \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \ 
	mkd="mkdir -pv"

# For removing files and folders with the immutable bit set
alias rmi='removeImmutable () {sudo chattr -i "$1"; sudo rm -rf "$1";}; removeImmutable'

# Prefer color output
alias \
	ls="ls -hN --color=auto --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi" \
	ip="ip -c" \
	tree="tree -C"

#Different versions of the ls command
alias \
	l="pwd; ls -Ahl" \
	ll="ls -hl"\
	la="ls -Ah"
	

# Aliases used for git:
alias \
	gdl="git -c color.ui=always diff | less -REX" \
	gfp="git fetch --all && git branch -D backup 2> /dev/null; git branch backup && git reset --hard && git pull" \ # A force pull with backup
	gss="git -c color.status=always status | head -n 30" \
	gsl="git -c color.status=always status | less -REX" \
	gl1="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all" \
	gl2="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all" \
	gcount="git log --oneline | wc -l" \


# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	map="setxkbmap" \
	e="devour neovide --nofork" \
	clip="xclip -selection clipboard" \
	g="git " \
  vsrc="source venv/bin/activate"

# For testing the internet connection
alias \
	ts="ping 1.1.1.1" \
	tsd="ping google.com"

# Also git, but for bare repo
alias dot-git='/usr/bin/git --git-dir=$HOME/.dootfiles.git/ --work-tree=$HOME'
alias dot-config='doot-git config --local status.showUntrackedFiles no'
